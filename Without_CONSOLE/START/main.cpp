#include <Windows.h>
#include <iostream>
#include <filesystem>
#include <BlackBone/Process/Process.h>
using namespace std;
namespace fs = std::filesystem;
using namespace blackbone;
namespace AntiEAC_DBXV2 {
	fs::path path_x = fs::current_path();
	string an = "\\bin\\DBXV2.exe";
	string dbxv2 = path_x.string() + an;
}
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR lpCmdLine, int nCmdShow)
{
	Process proc;
	const wstring currentpath = fs::current_path().wstring() + L"\\bin\\DBXV2.exe";
	NTSTATUS st = proc.CreateAndAttach(currentpath);
	if (st) {
		MessageBoxA(0, "FAILED!!!", "DBXV2 NOEAC", 0);
	}
	else {
		MessageBoxA(0, "SUCCESS!!!", "DBXV2 NOEAC", 0);
	}
	return 0;
}