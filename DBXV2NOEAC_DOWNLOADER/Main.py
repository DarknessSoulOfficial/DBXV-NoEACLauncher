import tqdm
import os
import sys
import requests as req #Getting Request Status For Downloading :D

class DownloadClass:
    def Download(url_name : str, filename : str):
        with open(filename, "wb") as o:
            with req.get(url_name, stream=True) as r:
                r.raise_for_status()
                total = int(r.headers.get('content-length', 0))
                tqdm_params = {
                    'desc': url_name,
                    'total': total,
                    'miniters': 1,
                    'unit_scale': True,
                    'unit_divisor': 1024,
                }
                with tqdm.tqdm(iterable=tqdm_params, desc=str("Downloading..."), ascii=True) as pb:
                    for chunk in r.iter_content(chunk_size=1):
                        pb.update(len(chunk))
                        o.write(chunk)
            print("Download Is Completed!!!")
def Main():
    DownloadClass.Download("https://gitlab.com/DarknessSoulOfficial/DBXV-NoEACLauncher/-/raw/main/Bin_Release/START.exe?ref_type=heads&inline=false", "START.exe")

if __name__ == "__main__":
    Main()
